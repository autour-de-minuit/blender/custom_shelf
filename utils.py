import bpy
import os
from os import listdir,mkdir,scandir
from os.path import join,dirname,splitext,isdir,exists,basename
from bpy.types import Operator, Panel, PropertyGroup
from bpy.props import *
import subprocess
import json


id_type = {"Action" : "actions", "Armature":"armatures", "Brush":"brushes", "CacheFile":"cache_files", "Camera":"cameras",
            "Curve":"curves", "FreestyleLineStyle":"linestyles", "GreasePencil":"grease_pencil", "Group":"groups",
            "Image":"images", "Key":"shape_keys", "Light":"lights", "Lattice":"lattices", "Library":"librairies", "Mask":"masks",
            "Material":"materials", "Mesh":"meshes", "MetaBall":"metaballs", "MovieClip":"movieclips", "NodeTree":"node_groups",
            "Object":"objects", "PaintCurve":"paint_curves", "Palette":"palettes", "ParticleSettings":"particles",
            "Scene":"scenes", "Screen":"screens", "Sound":"sounds", "Speaker":"speakers", "Text":"texts", "Texture":"textures",
            "VectorFont":"fonts", "WindowManager":"window_managers", "World":"worlds"}



def report(var,message,type='INFO') :
    print([a for a in locals()])
    print([a for a in globals()])


    if 'self' in var :
        var['self'].report({type}, message)

    else :
        print('')
        print(type)
        print(message)

def read_json(path):
    jsonFile = path
    if os.path.exists(jsonFile):
        try :
            with open(jsonFile) as data_file:
                return (json.load(data_file))
        except :
            print("the file %s not json readable"%jsonFile)
            return



def search_filter(search,str) :
    return search.lower() in str.lower()

def title(string) :
    return string.replace('_',' ').replace('-',' ').title()

def arg_name(string) :
    return string.replace(' ','_').replace('-','_').lower().strip('_')

def open_folder(path) :
    try:
        os.startfile(path)
    except:
        subprocess.Popen(['xdg-open', path])


def dic_to_args(dic):
    import collections
    args = collections.OrderedDict()

    for k,v in dic.items() :
        if k in ('icon','description') or not isinstance(k,str):
            continue

        if isinstance(v,str) :
            if '/' in v or '\\' in v :
                args[k] = StringProperty(default=v,subtype = "FILE_PATH")
            else :
                args[k] = StringProperty(default=v)

        elif isinstance(v,bool) :
            args[k] = BoolProperty(default=v)

        elif isinstance(v,float) :
            args[k] = FloatProperty(default=v,precision=3)

        elif isinstance(v,int) :
            args[k] = IntProperty(default=v)

        elif isinstance(v,dict) :
            if v.get("items") and isinstance(v["items"],(tuple,list)) :
                args[k] = EnumProperty(items = [(i,i,"") for i in v["items"]])

            elif v.get("collection") and isinstance(v["collection"],str) :
                if v["collection"] not in id_type :
                    print("Collection %s not supported must be in %s"%(v["collection"],id_type))
                    #args[k] = PointerProperty(type = getattr(bpy.types,v["collection"]))
                else :
                    args[k] = PointerProperty(type = getattr(bpy.types,v["collection"]),poll = v.get('poll'))


    return args


def read_info(script_path) :
    import collections

    if isinstance(script_path,list) :
        lines  =script_path
    else :
        with open(script_path,encoding="utf-8") as f:
            lines = f.read().splitlines()

    info =  {}
    for i,l in enumerate(lines) :
        if i >= 10 :
            return info,lines

        if l.startswith("info") :
            info_start = i
            info_str = l

            while info_str.endswith(',') or not info_str.endswith('}') or i == len(lines)-1:
                i+=1
                info_str+= lines[i]

            info_end = i

            opening_brace_index = info_str.find('{')

            try :
                info = eval(info_str[opening_brace_index:])
            except :
                print('The info header in the file %s cannot be read'%script_path)
                break

            info = [(k,v) for k,v in info.items()]
            info.sort(key = lambda x : info_str.find(x[0]))

            info = collections.OrderedDict(info)

            del lines[info_start:info_end+1]

            break

    # remove backspace
    for i in range(len(lines)) :
        if lines[0] :
            break
        else :
            lines.pop(0)

    for i in range(len(lines)) :
        if lines[-1] :
            break
        else :
            lines.pop(-1)

    return info,lines

def dic_to_str(dic,keys= None,spaces = 4) :
    if not keys :
        keys = sorted(dic)

    line = '{\n'
    for k in keys :
        v = dic[k]

        line += ' '*spaces

        if isinstance(k,str) :
            line +="'%s'"%k
        else :
            line += "%s"%k

        line += ' : '

        if isinstance(v,str) :
            line +="'%s'"%v
        else :
            line += "%s"%v

        line += ',\n'

    line += '}\n\n'

    return line
