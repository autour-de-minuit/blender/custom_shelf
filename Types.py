import bpy
from bpy.types import Operator, Panel, PropertyGroup
from .utils import *
from bpy.props import *
from .properties import CustomShelfProps


class Types():
    def initialyse(self,label) :
        package = __package__.replace('_','').replace('-','').lower()
        idname = label.replace(' ','_').replace('-','_').lower()

        self.bl_label = title(label)
        self.bl_idname = '%s.%s'%(package,idname)

        #self.bl_props = bpy.context.scene.CustomShelf
        #self.prefs = ""




class CSHELF_OT_shelf_popup(Types,Operator) :
    #props = None
    @property
    def props(self) :
        #print(getattr(bpy.context.window_manager.CustomShelf,self.prop))
        return getattr(bpy.context.window_manager.CustomShelf,self.prop)

    def get_props(self):
        properties = self.props.bl_rna.properties
        #props =
        return {k:v for k,v in self.args.items() if k in properties}



    def set_props(self,prop,value):
        setattr(bpy.context.window_manager.CustomShelf,prop,value)


    def initialyse(self,label,info) :
        super().initialyse(CSHELF_OT_shelf_popup,label)

        self.args = dic_to_args(info)


        Props = type("props",(PropertyGroup,),{'__annotations__' : dict(self.args)})
        bpy.utils.register_class(Props)

        setattr(CustomShelfProps,arg_name(label),PointerProperty(type=Props))
        #self.props = getattr(bpy.context.window_manager,self.bl_idname)
        self.info = info
        self.prop = arg_name(label)



        #print(self.args)



        #properties = self.get_props().bl_rna.properties
        #props = {k:v for k,v in self.args.items() if k in properties or hasattr(v,"bl_idname")}

        return #props

    def invoke(self, context, event):
        if event.alt :
            for t in [t for t in bpy.data.texts if t.filepath == self.script] :
                bpy.data.texts.remove(t)

            text = bpy.data.texts.load(self.script)

            areas = []
            text_editor = None
            for area in context.screen.areas :
                if area.type == 'TEXT_EDITOR' :
                    text_editor = area

                else :
                    areas.append(area)
            if not text_editor :
                bpy.ops.screen.area_split(direction = "VERTICAL")

                for area in context.screen.areas :
                    if area not in areas :
                        text_editor = area
                        text_editor.type = "TEXT_EDITOR"
                        text_editor.spaces[0].show_syntax_highlight = True
                        text_editor.spaces[0].show_word_wrap = True
                        text_editor.spaces[0].show_line_numbers = True

                        context_copy = context.copy()
                        context_copy['area'] = text_editor
                        #bpy.ops.text.properties(context_copy)
                        #bpy.ops.view3d.toolshelf(context_copy)

            text_editor.spaces[0].text = text

            return {"FINISHED"}
        else :
            if self.args :
                #set default value for collection
                for k,v in self.info.items() :
                    if isinstance(v,dict) and v.get('collection') :
                        collection = getattr(bpy.data, id_type[v["collection"]])

                        setattr(self.props, k,collection.get(v["value"]))



                return context.window_manager.invoke_props_dialog(self)
            else :
                return self.execute(context)


    def bl_report(self,*args) :
        if len(args) and args[0] in ('INFO','ERROR', 'WARNING') :
            return self.report({args[0]},' '.join(args[1:]))
        else :
            print(*args)

    def execute(self,context):
        import tempfile

        info,lines = read_info(self.script)

        values = dict(info)
        for arg in self.args :
            value = getattr(self.props,arg)

            if hasattr(value,"name") :
                values[arg] = {'value': value.name}

            elif isinstance(info.get(arg),dict) :
                values[arg] = {'value': value}

            else :
                values[arg] = value


        info_str = "info = %s\n\n"%values

        tmp_folder = tempfile.gettempdir()
        script_path = join(tmp_folder, "shelf.py")

        with open(script_path,"w",encoding = 'utf-8') as f :
            f.write(info_str+'\n'.join(lines))

        exec(compile(open(script_path,encoding = 'utf-8').read(), script_path, 'exec'),{'info' : values,'print':self.bl_report})

        return {'FINISHED'}

    def draw(self,context) :
        layout = self.layout
        bl_props = context.scene.CustomShelf

        #print(self.get_props())

        for k in self.args :
            #filter

            row = layout.row(align = True)
            row.label(text=title(k))

            row.prop(self.props,k,text='')
                #row.prop_search(self.props,k,self.props,k+"_prop_search",text='')
            #op = self.args[k+"_prop_search"]
            #row.operator(op.bl_idname,text="",icon ="COLLAPSEMENU" )


    def check(self,context) :
        return True



"""
class ShelfPropSearch(Types,Operator):
    bl_property = "enum"

    enum = None
    prop = ""

    def execute(self,context) :

        parent_props = getattr(bpy.context.window_manager.CustomShelf,self.parent.prop)

        setattr(parent_props,self.prop,self.enum)

        return {"FINISHED"}

    def initialyse(self,label,enum_list) :
        super().initialyse(ShelfPropSearch,"prop_search_"+label)

        self.enum = EnumProperty(items = [(i,i,"") for i in enum_list])
        self.prop = label
        #print(self.enum)

    def invoke(self, context, event):
        #print("invoke",self.parent)
        context.window_manager.invoke_search_popup(self)
        return {"FINISHED"}

"""


class CSHELF_PT_shelf_panel(Types,Panel):
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    #bl_category = "SHELF"
    settings = {}
    scripts = []

    @staticmethod
    def search_filter(search,str) :
        return search.lower() in str.lower()

    @classmethod
    def poll(self, context):
        bl_props = getattr(context.scene.CustomShelf,self.cat)
        prefs = bpy.context.preferences.addons[__package__].preferences
        search = bl_props.search
        shelf = [self.search_filter(search,s["text"]) for s in self.scripts]

        #print("settings",self.settings)
        is_taged = True
        if self.settings and self.settings.get('tags') :
            tag = prefs.tag_filter
            if not tag :
                tag = os.getenv("CUSTOM_SHELF_TAG")

            is_taged = tag in self.settings['tags']

        return any(shelf) and is_taged


    def draw(self,context) :
        layout = self.layout
        bl_props = getattr(context.scene.CustomShelf,self.cat)
        search = bl_props.search

        for script in self.scripts :
            args = script.copy()
            if not self.search_filter(search,script["text"]) : continue

            if bl_props.filter and script["text"].startswith('_') : continue

            args["text"] = title(script["text"])

            #print(script)
            layout.operator(**args)



class CSHELF_PT_shelf_header(Panel):
    bl_label = "Custom Shelf"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_options = {"HIDE_HEADER"}


    def draw(self, context):
        layout = self.layout
        bl_props = bpy.context.scene.CustomShelf
        prefs = bpy.context.preferences.addons[__package__].preferences

        cat_props = getattr(bl_props, self.cat)

        row = layout.row(align = True)

        row.operator_menu_enum("customshelf.set_tag_filter",'tag_filter',icon= "DOWNARROW_HLT",text='')
        row.operator("customshelf.refresh", text='',emboss=True,icon= "FILE_REFRESH")
        #row.separator()

        row.prop(cat_props, "filter",text='',emboss=True,icon= "FILTER")
        #row.separator()

        row.prop(cat_props,"search",icon = "VIEWZOOM",text='')
        #row.separator()

        row.operator("customshelf.open_shelf_folder", text='',emboss=True,icon= "FILE").path = self.path
