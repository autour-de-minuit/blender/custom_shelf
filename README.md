# Custom Shelf

Adds buttons to launch custom python scripts in the User panel.


## Installation

Download as zip and install via the preferences > addons panel
or clone repository in blender addon folder.

## Usage

Place your .py scripts in the `shelves/` folder with following folder structure:  

`shelves / sidebar_tab_name / panel_name / button_name.py`

Here is the dictionnary to put at the beginning of your code to have tooltip, icon 
and variables inside a popup:

```
info = {
    'icon' : 'SORTALPHA',
    'description' : 'Rename active object in something',
    'name' : 'Some name',
    'enum' : {'value': '3', 'items': ['1', '2', '3', '4']},
    'int' : 2,
    'path' : '/temp',
    'bool' : False,
    'float' : 2.0,
    'collection' : {'value': 'object', 'collection': 'Object'},
}
```


To get yout variables in your code :

`name = info['name']`

The collection return the name, so to get an object :

`ob = bpy.data.objects.get(info['collection']['value'])`


<!-- You can put a json file named "settings.json" inside your panel folder for tagging the folder

`{"tags" : ["td"]}` -->

You can next filter by tags by clicking on the arrow icon

Script file starting with '_' are hided by default, you can unhide them by unchecking the filter icon

## Support
Use the issues tracker to report a bug.

## License
GNU-GPL V3

