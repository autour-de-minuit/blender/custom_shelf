bl_info = {
    "name": "Custom Shelf",
    "author": "Christophe Seux",
    "description": "Adds buttons to launch custom python scripts in the User panel.",
    "version": (0, 3, 2),
    "blender": (2, 82, 0),
    "location": "View3D > User Panel",
    "doc_url": "https://gitlab.com/autour-de-minuit/blender/custom_shelf/-/blob/main/README.md",
    "tracker_url": "https://gitlab.com/autour-de-minuit/blender/custom_shelf/-/issues",
    "support": "COMMUNITY",
    "category": "User"}



if "bpy" in locals():
    import importlib
    importlib.reload(operators)
    importlib.reload(panels)
    importlib.reload(functions)
    importlib.reload(properties)

from .utils import report
from .functions import read_shelves
from .import operators
#from .import shelves
from .import properties
from .import panels
from .properties import CustomShelfSettings,CustomShelfProps

import bpy
import os


class_to_register = [
properties.AdditionnalShelves,
properties.CustomShelfProps,
properties.CustomShelfSettings,
properties.CustomShelfPrefs,
operators.CSHELF_OT_refresh,
operators.CSHELF_OT_add_shelf_folder,
operators.CSHELF_OT_remove_shelf_folder,
operators.CSHELF_OT_open_shelf_folder,
operators.CSHELF_OT_add_script,
operators.CSHELF_OT_set_tag_filter,
panels.CSHELF_PT_text_editor
]

def register():
    for bl_classes in class_to_register :
        bpy.utils.register_class(bl_classes)
    
    bpy.types.Scene.CustomShelf = bpy.props.PointerProperty(type=CustomShelfSettings)
    bpy.types.WindowManager.CustomShelf = bpy.props.PointerProperty(type=CustomShelfProps)

    env_shelves = os.getenv("CUSTOM_SHELVES")
    if env_shelves:
        shelves = env_shelves.split(os.pathsep)
        prefs = bpy.context.preferences.addons[__name__].preferences
        # prefs.global_shelves = ''

        # prefs.additionnal_shelves.clear()
        for path in shelves:
            s = next((s for s in prefs.additionnal_shelves if s.path == path), None)
            if not s:
                s = prefs.additionnal_shelves.add()
                s.path = path

    read_shelves()

def unregister():
    # unregister panel :
    for p in CustomShelfSettings.panel_list :
        try :
            bpy.utils.unregister_class(p)
        except :
            pass

    del bpy.types.Scene.CustomShelf
    del bpy.types.WindowManager.CustomShelf


    for bl_classes in class_to_register :
        bpy.utils.unregister_class(bl_classes)
