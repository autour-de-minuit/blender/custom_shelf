## How to add scripts

Create a folder for the `tab category` of your scripts and a subfolder for the `panel label` and place your `.py` inside so they can be displayed in the Sidebar

Structure : `shelves / sidebar_tab_name / panel_name / button_name.py`

## example

with: shelves/dev/custom_tools/fix_objects.py

The place in interface is `Dev` tab > `Custom Tools` panel >  button `Fix Objects`
