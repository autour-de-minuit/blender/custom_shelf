from .utils import *



class CSHELF_PT_text_editor(bpy.types.Panel):
    bl_space_type = "TEXT_EDITOR"
    bl_region_type = "UI"
    bl_category = "Dev"
    bl_label = "Custom Shelf"

    def draw(self, context):
        layout = self.layout
        shelves = bpy.context.scene.CustomShelf

        row = layout.row(align = True)

        row.operator("customshelf.add_script", text='Add Script',emboss=True,icon= "LONGDISPLAY")
        row.separator()



"""
class CustomShelfPanel(bpy.types.Panel):
    bl_label = "Custom Shelf"
    bl_category = "SHELF"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    # bl_context = "posemode"


    def draw_header(self, context):
        view = context.space_data
        layout = self.layout
        row=layout.row(align=True)
        row.operator("customshelf.refresh", \
            text='',emboss=False,icon= "FILE_REFRESH")


    def draw(self, context):
        layout = self.layout

        window = context.window
        scene = context.scene
        rd = scene.render

        shelves = bpy.context.scene.CustomShelf.folders

        for key,value in sorted(shelves.items()) :
            if key.startswith(('_','-')) :
                continue

            box = layout.box()
            #box.alignment='EXPAND'
            box_col = box.column(align =False)

            if value['expand'] == True :
                expandIcon = 'TRIA_DOWN'
            else :
                expandIcon = 'TRIA_RIGHT'

            row = box_col.row(align = True)

            row.operator("customshelf.expand",text=key.upper(), icon = expandIcon,emboss=False).folder = key

            #subrow = row.row(align = True)
            #subrow.alignment = 'CENTER'
            #subrow.label(key.upper())

            #col.separator()
            if value['expand'] == True :

                #text =' '.join([' ' for a in range(0,len(key))])
                #row.prop(context.scene.CustomShelf,key,emboss = False,text=' ')


                for script,settings in sorted(value['scripts'].items()) :
                    if script.startswith(('_','-')) :
                        continue
                    #print(s[1])
                    func = box_col.operator("customshelf.run_function", \
                        text=script,icon = settings['icon'])
                    func.path = settings['path']


                    #box_col.separator()


            #layout.separator()
                #col.separator()

"""
