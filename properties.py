from .utils import *
from bpy.props import *



class CustomShelfSettings(bpy.types.PropertyGroup) :
    panel_list = []
    #category_enum = EnumProperty(items = )
    #tab_enum = EnumProperty(items = lambda s,c : [(f,f,"") for f in s.tabs])

    #search = StringProperty(options = {"TEXTEDIT_UPDATE"})
    #filter = BoolProperty(default = True)
    #folders = PointerProperty(type= CustomShelfFolders)
    #variables = bpy.props.PointerProperty(type= CustomShelfVariables)

class CustomShelfProps(bpy.types.PropertyGroup) :
    pass

class AdditionnalShelves(bpy.types.PropertyGroup) :
    path: StringProperty(name="Path",subtype = 'FILE_PATH')


class CustomShelfPrefs(bpy.types.AddonPreferences):
    bl_idname = __package__

    global_path = join(dirname(__file__),'shelves')
    global_shelves : StringProperty(name="Shelves Path",subtype = 'DIR_PATH',default =global_path)
    additionnal_shelves : CollectionProperty(type = AdditionnalShelves)
    tag_filter : StringProperty()


    def draw(self, context):
        prefs = bpy.context.preferences.addons[__package__].preferences
        layout = self.layout
        layout.prop(self, "global_shelves")

        row = layout.row(align = True)
        row.operator("customshelf.add_shelves_folder",icon="ADD",text="")
        row.label(text='Additionnal Shelves folder')

        for i,shelf in enumerate(prefs.additionnal_shelves) :
            row = layout.row(align = True)
            row.prop(shelf, "path")
            row.operator("customshelf.remove_shelves_folder",icon="REMOVE",text="").index = i

        row = layout.row(align = True)
        row.prop(self, "tag_filter")
        row.operator_menu_enum("customshelf.set_tag_filter",'tag_filter',icon= "DOWNARROW_HLT",text='')
